#include <SoftwareSerial.h>                                                                                                         

#include <NewPing.h>                                                                                                                            
#define TRIGGER_PIN  5  // Arduino pin tied to trigger pin on the ultrasonic sensor.                                                                   
#define ECHO_PIN     16  // Arduino pin tied to echo pin on the ultrasonic sensor.                                                                           
#define MAX_DISTANCE 400 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.                                    
const int power = 19;
//NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.                                                                           



SoftwareSerial mySerial =  SoftwareSerial(2,3);                                                                                                                                          


void setup() {                                                                                                                                                                              
  Serial.begin(115200); // Open serial monitor at 115200 baud to see ping results.                                                                                                               
  Serial.println("Hello, world?");                                                                                                                                                                   

  mySerial.begin(115200);
  mySerial.println("Hello, world?");
  
  pinMode(power, OUTPUT);
  
    digitalWrite(power, HIGH);
  pinMode(TRIGGER_PIN,OUTPUT);
  pinMode(ECHO_PIN,INPUT); 
}



void loop() {
  
  delay(1000);
  unsigned int uS = 0;

  digitalWrite(TRIGGER_PIN,LOW);
  delayMicroseconds(10);
  digitalWrite(TRIGGER_PIN,HIGH);
  delayMicroseconds(15);
  digitalWrite(TRIGGER_PIN,LOW);
  delayMicroseconds(200);
  //unsigned long started = millis();
  //delay(10);

  unsigned long i = 0;

  unsigned long countTriggerHigh = 0;
  unsigned long  countTriggerLow = 0;
  unsigned long  countEchoHigh = 0;
  unsigned long  countEchoLow = 0;
  
  int echoHigh = 0;
  int newEchoHigh = 0;
  
  unsigned long startMs = 0, stopMs = 0, deltaMs = 0, startUs = 0, stopUs = 0, deltaUs = 0;
  float mm = 0.0;
  
  
  while (i < 200000)
  {
    digitalRead(ECHO_PIN) == HIGH? newEchoHigh = 1 : newEchoHigh = 0;
    

    if (echoHigh != newEchoHigh){
      if (newEchoHigh == 1 ){
        Serial.println("Start");
        startMs = millis();
        startUs = micros();
      }
      
      if (newEchoHigh == 0 ){
        stopMs = millis();
        stopUs = micros();
        deltaMs = stopMs - startMs;
        deltaUs = stopUs - startUs;
        Serial.println("Stop");
        
        Serial.print("Milliseconds: ");
        Serial.println(deltaMs);
        Serial.print("Microseconds: ");
        Serial.println(deltaUs);
        mm = (float) deltaUs / 5.8;
        Serial.print("mm: ");
        Serial.println(mm);
      }

    }
    echoHigh = newEchoHigh;

    i++;
  } 

        Serial.println("Stopped"); 
}
