#define TINY_GSM_MODEM_SIM800 //Tipo de modem que estamos usando
#include <TinyGsmClient.h>
#include <PubSubClient.h>
#include <TaskScheduler.h>
#include "esp_system.h"
#include <SPI.h>
#include <WiFiClientSecure.h>
#include <ArduinoOTA.h>
#include <WiFi.h>
#include <OneWire.h>
#include <DallasTemperature.h>

Scheduler userScheduler; // to control your personal task

#define NOMEOTA "testeee"
#define DEVICE_ID "testeee"
#define MQTT_SERVER "mqtt.iothiemann.com"
#define MQTT_PORT 1883
#define mqtt_user ""
#define mqtt_pass ""
#define POTA 0
#define PONOFF1 27
#define PONOFF2 26
#define PONOFF3 25
#define LEDVERMELHO 15
#define LEDVERDE 2
#define P1DIPLAY 21
#define P2DIPLAY 22
//#define P1IO1 14
//#define P2IO1 12
#define P1IO2 35
#define P2IO2 32
#define PENERGIA 13
#define OLED_RESET 1
#define GSM_PIN "3030"
char topicoE[30] = "";
char topico2[30] = "";
char topicoT[30] = "";
char topico3[30]="";
int z=0;
const char* ssid     = "FRITZ"; const char* password = "12345679";

//Canal serial que vamos usar para comunicarmos com o modem. Utilize sempre 1
HardwareSerial SerialGSM(1);
TinyGsm modemGSM(SerialGSM);
TinyGsmClient gsmClient(modemGSM);

void mqttCallback(char* topico3, byte* payload, unsigned int len);
String ppayload = "";
PubSubClient mqtt(MQTT_SERVER, MQTT_PORT, mqttCallback, gsmClient);

bool Pota = 1;
bool callback = 0;
bool Vonoff1;
bool Vonoff2;
bool Vonoff3;
bool Venergia = 1;
bool led_pisca_o = 0;
bool led_pisca_e = 0;
bool inicializa = 0;
//bool Vp1io1=0;
//bool Vp2io1=0;
bool Vp1io2;
bool Vp2io2;

uint64_t chipid;
char topico[30] = "";
char topicosub[30] = "";

OneWire oneWire(17);
DallasTemperature sensors(&oneWire);


void reconecta_mqtt();
void EnviaEstadoOutputMQTT(char* TOPICO_PUBLISH, char* valor);
void pisca_verde();
void pisca_vermelho();
void initWiFi();
void setupGSM();
void connectMQTTServer();
void temperatura();
void energia();
void energiaperiodico();


////////////////////////////////////////////////////////// WATCHDOG/////////////////////////////////
const int wdtTimeout = 210000;  //time in ms to trigger the watchdog
hw_timer_t *timer = NULL;

void IRAM_ATTR resetModule() {
  ets_printf("reboot\n");
  //STATUS=3;
  delay(10000);
  esp_restart();
}
//////////////////////////////////////////////////////////FIM WATCHDOG/////////////////////////////

void mqttCallback(char* topico3, byte* payload, unsigned int len) {

 char controle[30] = "";
 String calb = "";
 ppayload = "";
  for (int i = 0; i < len; i++) {
  //Serial.prin((char)payload[i]);
    calb += (char)payload[i];
    }

  calb.toCharArray(controle, calb.length() + 1);
  EnviaEstadoOutputMQTT(topico2, controle);
  if (calb == "temp"){
    temperatura();
    return;
  }
  else if(calb=="reSet"){
    digitalWrite(LEDVERMELHO, LOW);
    digitalWrite(LEDVERDE, HIGH);
    delay(360000);
    ESP.restart();      
    }
  }

Task Temperatura(360 * TASK_SECOND, TASK_FOREVER, &temperatura, &userScheduler, true);
//Task t2 (10 * TASK_SECOND, TASK_FOREVER, &pisca_verde, &userScheduler, true);
//Task t6 (10 * TASK_SECOND, TASK_FOREVER, &pisca_vermelho, &userScheduler, true);
Task Energia(3600 * TASK_SECOND, TASK_FOREVER, &energiaperiodico, &userScheduler, true);




void setup()
{
  Serial.begin(115200);
  Serial.println("dentro do setup");
  ///////////////////////////INICIALIZA WATCHDOG/////////////////////////////////////////
  timer = timerBegin(0, 80, true);                  //timer 0, div 80
  timerAttachInterrupt(timer, &resetModule, true);  //attach callback
  timerAlarmWrite(timer, wdtTimeout * 1000, false); //set time in us
  timerAlarmEnable(timer);
  ///////////////////////////FIM INICIALIZAÇÃO WATCHDOG/////////////////////////////////////////
  pinMode(LEDVERMELHO, OUTPUT);
  pinMode(LEDVERDE, OUTPUT);
  //pinMode(PONOFF1, INPUT);
  //pinMode(PONOFF2, INPUT);
  //pinMode(PONOFF3, INPUT_PULLDOWN);
  pinMode(PENERGIA, INPUT_PULLDOWN);

  //pinMode(P2IO2,INPUT_PULLDOWN);
  // pinMode(P1IO2, INPUT_PULLDOWN); // pode ser um adc
  pinMode(POTA, INPUT_PULLDOWN);
  //pinMode(P1IO1,INPUT_PULLDOWN);
  //  pinMode(P2IO1,INPUT_PULLDOWN);
  //  digitalWrite(P1IO1,HIGH);
  //Serial.begin(115200);
  delay(10);

  Venergia=digitalRead(PENERGIA);
  
  chipid = ESP.getEfuseMac();
  snprintf(topicosub, 23, "%04X%08X/conf", (uint16_t)(chipid >> 32), (uint32_t)chipid);
  snprintf(topico2, 23, "%04X%08X/status", (uint16_t)(chipid >> 32), (uint32_t)chipid);
  snprintf(topicoT, 23, "%04X%08X/tempe", (uint16_t)(chipid >> 32), (uint32_t)chipid);
  snprintf(topicoE, 23, "%04X%08X/energia", (uint16_t)(chipid >> 32), (uint32_t)chipid);
 
  for (int j = 0; j < 20; j++) {
    digitalWrite(LEDVERMELHO, LOW);
    digitalWrite(LEDVERDE, HIGH);
    delay(j * 25);
    digitalWrite(LEDVERMELHO, HIGH);
    digitalWrite(LEDVERDE, LOW);
    delay(500 - j * 25);
    
    if (digitalRead(POTA) == HIGH) {
      Pota = 0;
    }
  }

  if (false) {
    initWiFi();

    Serial.println("Dentro do OTA...");
    ArduinoOTA.setHostname(NOMEOTA);

    ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";
    })
    .onEnd([]() {
    })
    .onProgress([](unsigned int progress, unsigned int total) {
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) {} ////Serial.prin("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) {} // //Serial.prin("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) {} // //Serial.prin("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) {} ////Serial.prin("Receive Failed");
      else if (error == OTA_END_ERROR) {} ////Serial.prin("End Failed");
    });
    ArduinoOTA.begin();
  }
  else {
    timerWrite(timer, 0);
    SerialGSM.begin(9600, SERIAL_8N1, 26, 27, false);
    delay(10000);
    modemGSM.restart();   
    delay(20000);
    modemGSM.waitForNetwork();
    //modemGSM.gprsConnect("inlog.vivo.com.br");
    delay(5000);
    timerWrite(timer, 0);
    setupGSM(); //Inicializa e configura o modem GSM
    connectMQTTServer(); //Conectamos ao mqtt server
  }
}

void setupGSM()
{
  digitalWrite(LEDVERMELHO, LOW);
  digitalWrite(LEDVERDE, LOW);
  Serial.printf("Setup GSM...");

  if (modemGSM.isGprsConnected()) {
    z=0;
    return;
  }

  digitalWrite(LEDVERMELHO,HIGH);
  digitalWrite(LEDVERDE, LOW);
   if ( GSM_PIN && modemGSM.getSimStatus() != 3 ) {
   // modemGSM.simUnlock(GSM_PIN);
    
    digitalWrite(LEDVERMELHO,HIGH);
    digitalWrite(LEDVERDE,HIGH);
    digitalWrite(LEDVERMELHO,LOW);
    digitalWrite(LEDVERDE,HIGH);
    delay(100);
    digitalWrite(LEDVERMELHO,HIGH);
    digitalWrite(LEDVERDE,HIGH);
    delay(500);
    digitalWrite(LEDVERMELHO,LOW);
    digitalWrite(LEDVERDE,HIGH);
    delay(100);
    digitalWrite(LEDVERMELHO,HIGH);
    digitalWrite(LEDVERDE,HIGH);
    delay(500);
    digitalWrite(LEDVERMELHO,LOW);
    digitalWrite(LEDVERDE,HIGH);
    delay(100);
  }

  digitalWrite(LEDVERMELHO,HIGH);
  digitalWrite(LEDVERDE, LOW);

  
  if (!modemGSM.waitForNetwork(60000L)) {
  //modemGSM.init();
    for(int h=0;h<10;h++){
    digitalWrite(LEDVERMELHO,LOW);
    digitalWrite(LEDVERDE,HIGH);
    delay(250);
    digitalWrite(LEDVERMELHO,HIGH);
    digitalWrite(LEDVERDE,HIGH);
    delay(250);
    }
    modemGSM.restart();   
    delay(20000);
  }
  else{
    modemGSM.gprsConnect("virtueyes.com.br", "virtu", "virtu");//"datatem.algar.br", "datatem", "datatem"    
    for(int h=0;h<200;h++){
    digitalWrite(LEDVERMELHO,LOW);
    digitalWrite(LEDVERDE,HIGH);
    delay(250);
    digitalWrite(LEDVERMELHO,HIGH);
    digitalWrite(LEDVERDE,LOW);
    delay(250);
    }
    z++;
    if(z==10)  {
    modemGSM.restart();
    //modemGSM.init();
    delay(10000);
    z=0;  
    }
    }  

  //Mostra informação sobre o modem
 // Serial.prin(modemGSM.getModemInfo());
  timerWrite(timer, 0); //reset timer (feed watchdog)


  /*
  if (!modemGSM.isGprsConnected()) {
  while(!modemGSM.gprsConnect("datatem.algar.br", "datatem", "datatem")) {
    digitalWrite(LEDVERMELHO, LOW);
    digitalWrite(LEDVERDE, LOW);
    delay(500);
    digitalWrite(LEDVERMELHO, HIGH);
    digitalWrite(LEDVERDE, HIGH);
    delay(500);
  //modemGSM.gprsConnect("datatem.algar.br", "datatem", "datatem");
    connectMQTTServer();
   }
   return;
  }*/
}
void connectMQTTServer() {
   if (!modemGSM.isGprsConnected()) {
    return;
  }
  Serial.print("Connecting to MQTT Server...");
  //Se conecta ao device que definimos
  if (mqtt.connect(DEVICE_ID, mqtt_user, mqtt_pass)) {
    //Se a conexão foi bem sucedida
    Serial.print("Connected");
    mqtt.subscribe(topicosub, 1);
    mqtt.setCallback(mqttCallback);

  } else {
    //Se ocorreu algum erro
    Serial.print("error = ");
    Serial.print(mqtt.state());
    //delay(10000);
    //ESP.restart();
  }
}

void loop()
{
  timerWrite(timer, 0); //reset timer (feed watchdog)
  if (Pota == 0) {
    digitalWrite(LEDVERMELHO,HIGH);
    digitalWrite(LEDVERDE,LOW);
    userScheduler.execute();
    delay(10);
    digitalWrite(LEDVERMELHO,HIGH);
    digitalWrite(LEDVERDE,HIGH);

    ArduinoOTA.handle();
    
    delay(10);
  }
  else {
    
    reconecta_mqtt();
    mqtt.loop();
    userScheduler.execute();
    timerWrite(timer, 0); //reset timer (feed watchdog)
    energia();
    inicializa = 1;
    delay(10);
    digitalWrite(LEDVERMELHO,LOW);
    digitalWrite(LEDVERDE,LOW);
    delay(10);
    digitalWrite(LEDVERMELHO,HIGH);
    digitalWrite(LEDVERDE,HIGH);
  }
  timerWrite(timer, 0); //reset timer (feed watchdog)
}

void reconecta_mqtt() {

  if (!mqtt.connected()) {
    Serial.print("=== MQTT NOT CONNECTED ===");
    setupGSM();
    connectMQTTServer();
  }
}

void EnviaEstadoOutputMQTT(char* TOPICO_PUBLISH, char* valor)
{
  mqtt.publish(TOPICO_PUBLISH, valor, true);
  return ;
}


void pisca_verde() {

  if (led_pisca_e == 0) {
    digitalWrite(LEDVERDE, HIGH);
    led_pisca_e = 1;
  }
  else {
    digitalWrite(LEDVERDE, LOW);
    led_pisca_e = 0;
  }
  Serial.print("Dentro pisca");
  return;

}
void pisca_vermelho() {

  if (led_pisca_o == 0) {
    digitalWrite(LEDVERMELHO, HIGH);
    led_pisca_o = 1;
  }
  else {
    digitalWrite(LEDVERMELHO, LOW);
    led_pisca_o = 0;
  }
  Serial.print("Dentro pisca");
  return;

}

void initWiFi()
{

  //se já está conectado a rede WI-FI, nada é feito.
  //Caso contrário, são efetuadas tentativas de conexão
  if (WiFi.status() == WL_CONNECTED){
   Serial.println("Wifi conectado");
   
    return;
  }
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(100);
  }
}
void temperatura() {
  
  char controle[30] = "";
  String tempe = "";
  sensors.requestTemperatures();
  delay(100);
  while(sensors.getTempCByIndex(0)==85.00){
  sensors.requestTemperatures();
  //EnviaEstadoOutputMQTT(topicoT, "deu erro");
  delay(100);
  }
  tempe+= sensors.getTempCByIndex(0);
  //tempe+="}";
  tempe.toCharArray(controle, tempe.length() + 1);
  EnviaEstadoOutputMQTT(topicoT, controle);

}
  
void energia(){
char controle[30] = "";
  String energia = "";
if (digitalRead(PENERGIA) != Venergia) {
  delay(10);
  if (digitalRead(PENERGIA) != Venergia) {
    Venergia=digitalRead(PENERGIA);
    energia += "{\"Energia\":";
    energia+= Venergia;
    energia+="}";
    energia.toCharArray(controle, energia.length() + 1);
    EnviaEstadoOutputMQTT(topicoE, controle );
    } 
  }
}
void energiaperiodico(){
char controle[30] = "";
  String energia = "";
  Venergia=digitalRead(PENERGIA);
  energia += "{\"Energia\":";
  energia+= Venergia;
  energia+="}";
  energia.toCharArray(controle, energia.length() + 1);
  EnviaEstadoOutputMQTT(topicoE, controle );
}
