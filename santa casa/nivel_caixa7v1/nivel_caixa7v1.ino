//#define TINY_GSM_MODEM_SIM800 //Tipo de modem que estamos usando
#include <PubSubClient.h>
#include <TaskScheduler.h>
#include "esp_system.h"
#include <SPI.h>
#include <WiFiClientSecure.h>
#include <ArduinoOTA.h>
#include <WiFi.h>
#include <SoftwareSerial.h>
 
#define ULTRATEMP1 17
#define ULTRATEMP2 16
 

SoftwareSerial ss (ULTRATEMP1, ULTRATEMP2);   // RX, TX
 
const char* ssid     = "Rede Wi-Fi de Douglas"; const char* password = "fantasy7DG@";

 

byte hdr, data_h, data_l, chksum;
unsigned int distance;

uint64_t chipid;
char topico[30] = "";
char topicosub[30] = "";

void reconecta_mqtt();
void EnviaEstadoOutputMQTT(char* TOPICO_PUBLISH, char* valor);
void pisca_verde();
void pisca_vermelho();
void leitura(void);
void initWiFi();
void connectMQTTServer();
void energiaperiodico();
void energia();

void mqttCallback(char* topico2, byte* payload, unsigned int len);
String ppayload = "";
WiFiClient espClient;
PubSubClient mqtt(MQTT_SERVER, MQTT_PORT, mqttCallback, espClient);



////////////////////////////////////////////////////////// WATCHDOG/////////////////////////////////
const int wdtTimeout = 120000;  //time in ms to trigger the watchdog
hw_timer_t *timer = NULL;

void IRAM_ATTR resetModule() {
 // ets_printf("reboot\n");
  //STATUS=3;
  delay(10000);
  esp_restart();
}
//////////////////////////////////////////////////////////FIM WATCHDOG/////////////////////////////

Task Leitura(300 * TASK_SECOND, TASK_FOREVER, &leitura, &userScheduler, true);
 
//Task Energia(60 * TASK_SECOND, TASK_FOREVER, &energiaperiodico, &userScheduler, true);

void mqttCallback(char* topico2, byte* payload, unsigned int len) {

  // Only proceed if incoming message's topic matches
  if (String(topico2) == topicosub) {
    ppayload = "";
    for (int i = 0; i < len; i++) {
      ppayload += (char)payload[i];
    }
    if (ppayload == "nivel")
      leitura();
      return;
    }
  }


void setup()
{
    Serial.begin(115200);
       Serial.println("Setup Start");
  ///////////////////////////INICIALIZA WATCHDOG/////////////////////////////////////////
  timer = timerBegin(0, 80, true);                  //timer 0, div 80
  timerAttachInterrupt(timer, &resetModule, true);  //attach callback
  timerAlarmWrite(timer, wdtTimeout * 1000, false); //set time in us
  timerAlarmEnable(timer);
  ///////////////////////////FIM INICIALIZAÇÃO WATCHDOG/////////////////////////////////////////
 

  chipid = ESP.getEfuseMac();
  snprintf(topicosub, 23, "%04X%08X/conf", (uint16_t)(chipid >> 32), (uint32_t)chipid);
  snprintf(topico2, 23, "%04X%08X/conf", (uint16_t)(chipid >> 32), (uint32_t)chipid);
 
  userScheduler.execute();
  initWiFi();
    
  connectMQTTServer(); //Conectamos ao mqtt server
  timerWrite(timer, 0); //reset timer (feed watchdog)
    
   
       Serial.println("Setup End");
}
void connectMQTTServer() {
    Serial.println("MQTT Start");
  if (mqtt.connect(DEVICE_ID, mqtt_user, mqtt_pass)) {
    mqtt.subscribe(topicosub, 1);
    mqtt.setCallback(mqttCallback);

  } else {
    delay(10000);
    ESP.restart();
  }
    Serial.println("MQTT End");
}

void loop()
{
       Serial.println("loop Start");
    userScheduler.execute();
    timerWrite(timer, 0); //reset timer (feed watchdog)
 
    reconecta_mqtt();
    mqtt.loop();
    delay(100);
    //energia();
      Serial.println("loop end");
}


void reconecta_mqtt() {
  Serial.println("MQTT Recovery Start");
  if (!mqtt.connected()) {
    initWiFi();
    connectMQTTServer();
  }
    Serial.println("MQTT Recovery End");
}

void EnviaEstadoOutputMQTT(char* TOPICO_PUBLISH, char* valor)
{
  mqtt.publish(TOPICO_PUBLISH, valor, true);
  return ;
}


void leitura() {
   Serial.println("Sensor Start");
  userScheduler.execute();
  for(int n=0;n<=1;n++){
  timerWrite(timer, 0); //reset timer (feed watchdog)
 
  ss.begin(9600); 
  while(!ss.available()){
  delay(10);
  userScheduler.execute();
 
  }
  
  char controle[30] = "";
  String Scontrole = "";
  int l = 0;
  int caixa2[21];
  int menor = 0;
  float mcaixa2 = 0;
  int dist1[22];
  int i=0;


  while(i<=20){
  delay(10);
  userScheduler.execute();
 
  if (ss.available())
  {
    hdr = (byte)ss.read();
    if (hdr == 255)
    {
      data_h = (byte)ss.read();
      data_l = (byte)ss.read();
      chksum = (byte)ss.read();

      if (chksum == ((hdr + data_h + data_l)&0x00FF))
      {
        distance = data_h * 256 + data_l;
        caixa2[i]=distance/10;
        if(caixa2[i]>0)
        i++;  
      if (i==20){
        for (int j = 0; j < 21; j++) {
          for (int k = 0; k < 20; k++) {
            if (caixa2[k] < caixa2[k - 1]) {
               menor = caixa2[k - 1];
               caixa2[k - 1] = caixa2[k];
                caixa2[k] = menor;
              }
          }
       }
      mcaixa2 = caixa2[10];
      Scontrole += mcaixa2;
      Scontrole.toCharArray(controle, Scontrole.length() + 1);
      
      Scontrole = "";
   
      snprintf(topico, 23, "%04X%08X/N1", (uint16_t)(chipid >> 32), (uint32_t)chipid); /// ver se vai dar certo
      EnviaEstadoOutputMQTT(topico, controle);
      Serial.print(controle);

      
      
      }
    }
    }  
  }
  }
ss.end();
  }
 
   Serial.println("Sensor End");
 
}

 
 

void initWiFi()
{
    Serial.println("Wi-fi Start");
 
 
userScheduler.execute();
  //se já está conectado a rede WI-FI, nada é feito.
  //Caso contrário, são efetuadas tentativas de conexão
  
  if (WiFi.status() == WL_CONNECTED){
  
    return;
  }
  WiFi.mode(WIFI_STA);
  WiFi.config(INADDR_NONE, INADDR_NONE,INADDR_NONE,INADDR_NONE);
  WiFi.setHostname(hostname.c_str());
  WiFi.begin(ssid, password);
  delay(10);
  while (WiFi.status() != WL_CONNECTED)
  {
    userScheduler.execute();
    //delay(100);
    //WiFi.begin(ssid, password);
  }
 
  Serial.println("Wi-Fi End");
}

 
 
