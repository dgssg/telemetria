/// esssa para gravar nova

// #define SIM800L_IP5306_VERSION_20190610
// #define SIM800L_AXP192_VERSION_20200327
// #define SIM800C_AXP192_VERSION_20200609
 #define SIM800L_IP5306_VERSION_20200811

/////para apagar
#define DUMP_AT_COMMANDS

// Define the serial console for debug prints, if needed
#define TINY_GSM_DEBUG SerialMon


//////fim do apagar

#define SerialMon Serial
// Set serial for AT commands (to the module)
#define SerialAT  Serial1
#define TINY_GSM_MODEM_SIM800          // Modem is SIM800
#define TINY_GSM_RX_BUFFER      1024

#include <TaskScheduler.h>
#include "esp_system.h"
#include "DHTesp.h"
#include "utilities.h"
#include <TinyGsmClient.h>
#include <StreamDebugger.h>
#include <PubSubClient.h>
/////para apagar

#ifdef DUMP_AT_COMMANDS
StreamDebugger debugger(SerialAT, SerialMon);
TinyGsm modem(debugger);
#else
TinyGsm modem(SerialAT);
#endif

#define uS_TO_S_FACTOR 1000000ULL  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP  60        /* Time ESP32 will go to sleep (in seconds) */


////fim do apagar
const char apn[]      = "inlog.vivo.com.br"; // Your APN
const char gprsUser[] = "datatem"; // User
const char gprsPass[] = "datatem"; // Password
const char simPIN[]   = ""; // SIM card PIN code, if any

DHTesp dht;
Scheduler userScheduler; // to control your personal task
String hostname = "iot-sc-0002";
#define NOMEOTA "iot-sc-0002.1"
#define DEVICE_ID "iot-sc-0002.1"
#define MQTT_SERVER "mqtt.iothiemann.com"
#define MQTT_PORT 1883
#define mqtt_user ""
#define mqtt_pass ""

char topicoE[30] = "";
char topico2[30] = "";
char topicoT[30] = "";
char topicoH[30] = "";
char topico3[30]="";

uint64_t chipid;
char topico[30] = "";
char topicosub[30] = "";

const int powerdht = 18; //alimentação do dht
int dhtPin = 19; // dados dht11

float Temperature;
float Humidity;



TinyGsmClient client(modem);
//TinyGsmClient gsmClient(modemGSM)
String ppayload = "";

void setupModem();
void turnOffNetlight();
void turnOnNetlight();

void conectaModem();
void mqttCallback(char* topico3, byte* payload, unsigned int len);
void reconecta_mqtt();
void EnviaEstadoOutputMQTT(char* TOPICO_PUBLISH, char* valor);
void pisca_verde();
void pisca_vermelho();
void connectMQTTServer();
void temperatura();
void energia();
void energiaperiodico();
void initTemp();

PubSubClient mqtt(MQTT_SERVER, MQTT_PORT, mqttCallback, client);

const int wdtTimeout = 210000;  //time in ms to trigger the watchdog
hw_timer_t *timer = NULL;

void IRAM_ATTR resetModule() {
  ets_printf("reboot\n");
  //STATUS=3;
  delay(10000);
  esp_restart();
}

Task Temperatura(300 * TASK_SECOND, TASK_FOREVER, &temperatura, &userScheduler, true);
//Task t2 (1 * TASK_SECOND, TASK_FOREVER, &pisca_verde, &userScheduler, true);
//Task t6 (1000 * TASK_MILLISECOND, TASK_FOREVER, &pisca_vermelho, &userScheduler, false);
Task Energia(3600 * TASK_SECOND, TASK_FOREVER, &energiaperiodico, &userScheduler, true);


void setup()
{

    // Set console baud rate
    SerialMon.begin(115200);

    delay(10);
    pinMode(5, OUTPUT);
    // Start power management
    if (setupPMU() == false) {
        Serial.println("Setting power error");
    }
// Some start operations
    setupModem();

    // Set GSM module baud rate and UART pins
    SerialAT.begin(115200, SERIAL_8N1, MODEM_RX, MODEM_TX);
    timer = timerBegin(0, 80, true);                  //timer 0, div 80
    timerAttachInterrupt(timer, &resetModule, true);  //attach callback
    timerAlarmWrite(timer, wdtTimeout * 1000, false); //set time in us
    timerAlarmEnable(timer);
    initTemp();

    chipid = ESP.getEfuseMac();
    snprintf(topicosub, 23, "%04X%08X/conf", (uint16_t)(chipid >> 32), (uint32_t)chipid);
    snprintf(topico2, 23, "%04X%08X/status", (uint16_t)(chipid >> 32), (uint32_t)chipid);
    snprintf(topicoT, 23, "%04X%08X/t1", (uint16_t)(chipid >> 32), (uint32_t)chipid);
    snprintf(topicoH, 23, "%04X%08X/h1", (uint16_t)(chipid >> 32), (uint32_t)chipid);
    snprintf(topicoE, 23, "%04X%08X/e", (uint16_t)(chipid >> 32), (uint32_t)chipid);
    
    timerWrite(timer, 0);

    printf(topicosub);
    printf(topico2);
    printf(topicoT);
    printf(topicoH);
    printf(topicoE);
    
}

void loop(){
  conectaModem();
  timerWrite(timer, 0); //reset timer (feed watchdog)
  reconecta_mqtt();
  mqtt.loop();
  userScheduler.execute();
  //energia();
  delay(10000);
  
  }

void setupModem()
{
#ifdef MODEM_RST
    // Keep reset high
    pinMode(MODEM_RST, OUTPUT);
    digitalWrite(MODEM_RST, HIGH);
#endif

    pinMode(MODEM_PWRKEY, OUTPUT);
    pinMode(MODEM_POWER_ON, OUTPUT);

    // Turn on the Modem power first
    digitalWrite(MODEM_POWER_ON, HIGH);

    // Pull down PWRKEY for more than 1 second according to manual requirements
    digitalWrite(MODEM_PWRKEY, HIGH);
    delay(100);
    digitalWrite(MODEM_PWRKEY, LOW);
    delay(1000);
    digitalWrite(MODEM_PWRKEY, HIGH);

    // Initialize the indicator as an output
    pinMode(LED_GPIO, OUTPUT);
    digitalWrite(LED_GPIO, LED_OFF);
}

void turnOffNetlight()
{
    SerialMon.println("Turning off SIM800 Red LED...");
    modem.sendAT("+CNETLIGHT=0");
}

void turnOnNetlight()
{
    SerialMon.println("Turning on SIM800 Red LED...");
    modem.sendAT("+CNETLIGHT=1");
}
void conectaModem(){
 // Restart takes quite some time
    // To skip it, call init() instead of restart()

    if (modem.isNetworkConnected()) {
        SerialMon.println("Network connected");
    }
    else{
    SerialMon.println("Initializing modem...");
    turnOffNetlight();
    modem.restart();
    
    // Turn off network status lights to reduce current consumption
    turnOffNetlight();

    // The status light cannot be turned off, only physically removed
    //turnOffStatuslight();

    // Or, use modem.init() if you don't need the complete restart
    String modemInfo = modem.getModemInfo();
    SerialMon.print("Modem: ");
    SerialMon.println(modemInfo);

    // Unlock your SIM card with a PIN if needed
    if (strlen(simPIN) && modem.getSimStatus() != 3 ) {
        modem.simUnlock(simPIN);
    }

    SerialMon.print("Waiting for network...");
    if (!modem.waitForNetwork(360000L)) {
        SerialMon.println(" fail");
        delay(10000);
        return;
    }
    SerialMon.println(" OK");

    // When the network connection is successful, turn on the indicator
    digitalWrite(LED_GPIO, LED_ON);

    if (modem.isNetworkConnected()) {
        SerialMon.println("Network connected");
    }

    SerialMon.print(F("Connecting to APN: "));
    SerialMon.print(apn);
    if (!modem.gprsConnect(apn, gprsUser, gprsPass)) {
        SerialMon.println(" fail");
        delay(10000);
        return;
    }


    }
    delay(1000);
    SerialMon.println(" OK");
  
}

void mqttCallback(char* topico3, byte* payload, unsigned int len) {

 char controle[30] = "";
 String calb = "";
 ppayload = "";
  for (int i = 0; i < len; i++) {
  //Serial.prin((char)payload[i]);
    calb += (char)payload[i];
    }

  calb.toCharArray(controle, calb.length() + 1);
  EnviaEstadoOutputMQTT(topico2, controle);
  if (calb == "temp"){
    temperatura();
    return;
  }
  else if(calb=="reSet"){
    delay(360000);
    ESP.restart();      
    }
  }
  void initTemp() {
    
  pinMode(powerdht, OUTPUT);
digitalWrite(powerdht, HIGH);
  byte resultValue = 0;
//digitalWrite(5, HIGH);  // Initialize temperature sensor
  dht.setup(dhtPin, DHTesp::DHT11);
  Serial.println("DHT initiated");

}
void connectMQTTServer() {
   if (!modem.isGprsConnected()) {
    return;
  }
  //Serial.prin("Connecting to MQTT Server...");
  //Se conecta ao device que definimos
  if (mqtt.connect(DEVICE_ID, mqtt_user, mqtt_pass)) {
    //Se a conexão foi bem sucedida
    //Serial.prin("Connected");
    mqtt.subscribe(topicosub, 1);
    mqtt.setCallback(mqttCallback);

  } 
}
void reconecta_mqtt() {

  if (!mqtt.connected()) {
    //Serial.prin("=== MQTT NOT CONNECTED ===");
    setupModem();
    connectMQTTServer();
  }
}

void EnviaEstadoOutputMQTT(char* TOPICO_PUBLISH, char* valor)
{
  mqtt.publish(TOPICO_PUBLISH, valor, true);
  return ;
}
void temperatura() {

  TempAndHumidity newValues = dht.getTempAndHumidity();
  // Check if any reads failed and exit early (to try again).
  /*String(newValues.temperature);
  String(newValues.humidity);
  */
  char controle[30] = "";
  String tempe = "";
  
  tempe= newValues.temperature;
  tempe.toCharArray(controle, tempe.length() + 1);
 
  EnviaEstadoOutputMQTT(topicoT,controle);
  tempe="";
  tempe=newValues.humidity;
  tempe.toCharArray(controle, tempe.length() + 1);
  EnviaEstadoOutputMQTT(topicoH, controle);
}

void energia(){
char controle[30] = "";
  String energia = "";
//if (digitalRead(PENERGIA) != Venergia) {
  delay(10);
//  if (digitalRead(PENERGIA) != Venergia) {
//    Venergia=digitalRead(PENERGIA);
//    energia+= Venergia;
//    energia.toCharArray(controle, energia.length() + 1);
    EnviaEstadoOutputMQTT(topicoE, controle );
//    } 
//  }
}
void energiaperiodico(){
char controle[30] = "";
  String energia = "";
//  Venergia=digitalRead(PENERGIA);
//  energia+= Venergia;
//  energia.toCharArray(controle, energia.length() + 1);
  EnviaEstadoOutputMQTT(topicoE, controle );
}
